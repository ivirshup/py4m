import pandas as pd
import numpy as np

# Bioinformatics read transformations

def get_lengths(path):
    """Given a `*count_annotation.txt` path, return a pd.Series mapping of transcript id to length."""
    table = pd.read_table(path)
    lengths = table["Length"]
    lengths.index = table["GeneID"]
    return lengths

def cpm(data, scaling_factors=1):
    library_size = data.sum() * scaling_factors * (10 ** -6)
    return data / library_size

def rpkm(data, length, scaling_factors=1):
    """
    Reads per kilo base per million mapped reads.

    Args:
        data (pd.DataFrame):
            Dataframe of read counts to transform. Columns are samples,
            index of transcript identifiers, values are ints.
        lengths (pd.Series):
            Series with index of transcript identifiers and values of
            transcript lenghts (int).

    Returns:
        (pd.DataFrame) of same shape as data, but with datatype float.
    """
    cpm_data = cpm(data, scaling_factors)
    length_kb = length / 1000
    return cpm_data.apply(lambda x: x / length_kb)

from rpy2.robjects import pandas2ri
pandas2ri.activate()
import rpy2.robjects as robjects
from rpy2.robjects.packages import importr

limma = importr("limma")
edgeR = importr("edgeR")
deseq2 = importr("DESeq2")

def calc_norm_factors(data, norm_method="TMM"):
    rdata = pandas2ri.py2ri(data)
    rdata_matrix = robjects.r["as.matrix"](rdata)
    norm_factors = edgeR.calcNormFactors(rdata_matrix, method=norm_method)
    return np.array(norm_factors)

def deseq_norm_factors(data):
    rdata = pandas2ri.py2ri(data)
    rdata_matrix = robjects.r["as.matrix"](rdata)
    norm_factors = deseq2.estimateSizeFactorsForMatrix(rdata)
    return np.array(norm_factors)

def normalize_between_arrays(data, norm_method="quantile"):
    rdata = pandas2ri.py2ri(data)
    rdata_matrix = robjects.r["as.matrix"](rdata)
    rnormed = limma.normalizeBetweenArrays(rdata_matrix, norm_method)
    normed = pandas2ri.ri2py_dataframe(rnormed)
    normed.index = data.index
    normed.columns = data.columns
    return normed

def yugene(column):
    """
    Applies YuGene transformation to series. Meant for and tested on RNA-seq.

    Usage:
        yugene_tranformed = raw_data.apply(yugene)
    """
    assert column.min() >= 0, "All values must be positive"
    column = column.sort_values(ascending=False)
    total = column.sum()
    cumsum = column.cumsum()
    ranked = column.rank(ascending=False)
    rankedvals = cumsum.groupby(ranked).min()
    yg = 1 - (ranked.map(rankedvals) / total)
    yg.loc[yg < 0] = 0
    return yg

# PCA stuff. Not really normalization, but not sure where to put it for now.

from sklearn.decomposition import PCA
from functools import singledispatch

@singledispatch
def get_pc_genes(data, pc_num, ndeviations=2, which="all"):
    """Tries to get important genes for a principal component."""
    pca = PCA()
    fit = pca.fit_transform(data.transpose())
    return get_pc_genes(pca, pc_num, ndeviations, which)

@get_pc_genes.register(PCA)
def get_pc_genes_pca(pca, pc_num, ndeviations=2, which="all"):
    pc = pca.components_[pc_num,:]
    assert which in ["all", "up", "down"]
    if which == "all":
        out = np.abs(pc - pc.mean()) > ndeviations*pc.std()
    elif which == "up":
        out = (pc - pc.mean()) > ndeviations*pc.std()
    elif which == "down":
        out = (pc - pc.mean()) < ndeviations*pc.std()
    return out
