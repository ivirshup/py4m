#!/usr/local/bin/python3
from pathlib import Path
from functools import singledispatch
from math import pi

import pandas as pd
import numpy as np
from sklearn.decomposition import PCA

from bokeh.plotting import figure, show
from bokeh.charts import Bar
from bokeh.layouts import column, row
from bokeh.palettes import viridis
from bokeh.models import ColumnDataSource, Range1d, Select, CustomJS, HoverTool
from bokeh.models import LabelSet, FuncTickFormatter, FixedTicker

####################################################
# Dendogram generation & management
####################################################
from scipy.spatial import distance
from scipy.cluster import hierarchy
from collections import Counter

def get_clusts(counts):
    """How clusters are generated under s4m."""
    covs = (2 ** counts).std(1) / (2 ** counts).mean(1)
    covs_DE = covs[covs > 0.1]
    dists = distance.pdist(np.transpose(counts[covs > 0.1]), "euclidean")
    return hierarchy.average(dists)

def groupings(clusts):
    """
    Given the output from SciPy's hierarchical clustering,
    returns list of groupings for each node.
    """
    total = len(clusts)
    groups = []
    for clust in clusts:
        cur = 0
        contains_idx = [int(x) for x in clust[0:2]]
        while cur < len(contains_idx):
            if contains_idx[cur] <= total:
                cur += 1
                continue
            node = contains_idx.pop(cur) - (total + 1)
            contains_idx.extend([int(x) for x in clusts[node, 0:2]])
        groups.append(contains_idx)
    assert ([len(x) for x in groups] == clusts[:, 3].astype(int)).all()
    return groups

def mappings(groupings, color_by):
    """Apply's map to nested list."""
    return [[color_by[g] for g in group] for group in groupings]

def unique_mappings(groupings, color_by, missing=None):
    """
    If a group has a unique color, returns the mapping from
    color_by, else missing.
    """
    maps = []
    for group in groupings:
        v = set(color_by[g] for g in group)
        if len(v) == 1:
            maps.append(v.pop())
        else:
            maps.append(missing)
    return maps

def format_clusts(clusts):
    """Returns a dendogram, and list of elements inside each node."""
    # TODO: Try to sort field of dendrogram instead of groups, indexing would be simpler
    dend = hierarchy.dendrogram(clusts, no_plot=True)
    dend2clust_idx = np.argsort([x[1] for x in dend["dcoord"]])
    clust2dend_idx = np.argsort(dend2clust_idx)
    groups = np.array(groupings(clusts))[clust2dend_idx]
    return dend, groups

####################################################
# Plotting helpers
####################################################

# Colors
def gen_color_map(lst, f=viridis):
    keys = sorted(set(lst))
    colors = f(len(keys))
    return dict(zip(keys, colors))

def gen_colors(lst, f=viridis):
    """
    Given an iterable and a coloring function, returns a list of colors.

    Args:
        lst (iterable):
            Iterable to color by.
        f (function):
            Function which takes one argument, the number of colors to generate.
    """
    mapping = gen_color_map(lst, f)
    return [mapping[x] for x in lst]

#####################################################
# ColumnDataSource
#####################################################

def gen_fields(targets):
    """Approximation of desired fields from targets file."""
    return [x for x in targets.columns if (x != "SampleID") & ("ReadFile" not in x)]

def gen_color_dsource(targets, fields):
    return ColumnDataSource(targets[fields].apply(gen_colors))

def gen_pca_dsource(pca_fit):
    gen = (("PC{}".format(i+1), list(pca_fit[:,i])) for i in range(0, pca_fit.shape[1]))
    return ColumnDataSource(dict(gen))

def gen_legend_dsource(plot_data):
    colors = plot_data.data["color"]
    selected = plot_data.data["coloredby"]
    unique_colors = []
    unique_selected = []
    for c, s in zip(colors, selected):
        if c not in unique_colors:
            unique_colors.append(c)
            unique_selected.append(s)
    legend_dsource = ColumnDataSource({
        "index":    list(range(len(unique_colors), 0, -1)),
        "color":    unique_colors,
        "coloredby": unique_selected
        })
    # TODO Make unsetting selection on legend or pca unset selection on both
    legend_dsource.callback = CustomJS(args=dict(plot_data=plot_data, legend_data=legend_dsource), code="""
        var inds = cb_obj.get("selected")["1d"].indices;
        var plot_selected = plot_data.get("selected")
        var plot_coloredby = plot_data.get("data")["coloredby"]
        var legend_coloredby = legend_data.get("data")["coloredby"]
        var selected = []
        for (i=0; i<inds.length; i++){
            selected.push(legend_coloredby[inds[i]])
        }
        var plot_inds = plot_selected["1d"].indices
        for (i=0; i<plot_coloredby.length; i++){
            if (selected.includes(plot_coloredby[i]) && !plot_inds.includes(i)) {
                plot_inds.push(i)
            }
        }
        inds = []
        plot_selected["1d"].indices = plot_inds
        plot_data.trigger("change")
        cb_obj.trigger("change")
    """)
    return legend_dsource

def box_quantiles(data):
    return { "Qmin":data.quantile(0).values,
             "Q1":data.quantile(0.25).values,
             "Q2":data.quantile(0.5).values,
             "Q3":data.quantile(0.75).values,
             "Qmax":data.quantile(1).values }
# This could def be more elegant. Maybe customizable based on which plots were added?
# Each plot type adds it's own info?
def gen_plot_dsource(data, targets, fields, pca_data, color_data):
    pdata = {}
    pdata["idx"] = range(0, data.shape[1]) # I thin
    pdata.update({"X": pca_data.data["PC1"], "Y": pca_data.data["PC2"]})
    pdata["color"] = color_data.data[fields[0]]
    pdata["coloredby"] = targets[fields[0]]
    pdata["SampleID"] = targets["SampleID"]
    pdata.update(targets[fields].to_dict("list"))
    pdata.update(box_quantiles(data))
    return ColumnDataSource(pdata)

def gen_dendcolor_ds(groups, targets, fields):
    t = targets[fields]
    colors = {}
    for name, data in t.iteritems():
        maps = unique_mappings(groups, data.astype(str), missing="")
        colors[name] = list(gen_colors(maps))
    return ColumnDataSource(colors)

def gen_dend_hover(targets, fields):
    ttips = []
    ttips.append(("index", "$index"))
    ttips.extend([(f,"@{}_str".format(f)) for f in fields])
    # print(ttips)
    return HoverTool(tooltips=ttips)

def gen_dend_selector(fields, plot_ds, color_ds):
    selector = Select(title="Color dendogram by:",
                      value=fields[0],
                      options=fields)
    selector.callback = CustomJS.from_coffeescript(
        args=dict(plot_data=plot_ds, color_data=color_ds),
        code="""
        data = plot_data.get("data")
        colors = color_data.get("data")
        selection = cb_obj["value"]
        data["color"] = colors[selection]
        data["coloredby"] = data[selection]
        plot_data.trigger("change")
        """)
    return selector

def gen_dend_ds(groups, dend, targets, fields, color_dsource):
    """
    Returns datasource used for plotting dendogram. Should have coords and
    colors at minumum
    """
    t = targets[fields]
    dsdf = pd.DataFrame()
    maps = {}
    for name, data in t.iteritems():
        maps[name] = mappings(groups, data.astype(str))
    dsdf["x"] = dend["icoord"]
    dsdf["y"] = dend["dcoord"]
    for f, l in maps.items():
        dsdf[f] = np.array([Counter(x) for x in l])
        dsdf[f+"_str"] = dsdf[f].apply(lambda x: str(dict(x)))
    dsdf["groups"] = groups # For now, just so it's there
    dsdf["color"] = color_dsource.data[fields[0]]
    dsdf["coloredby"] = dsdf[fields[0]]
    return ColumnDataSource(dsdf)


#####################################################
# Styling helpers
#####################################################
# Hovertool
def gen_hovertool(targets, fields=None):
    """Given a targets DataFrame return hover tools for bokeh plots."""
    if fields is None:
        fields = filter(lambda x: x not in ("ReadFile1", "ReadFile2"), targets.columns)
    hover = HoverTool( tooltips=[(field, "@"+field) for field in fields] )
    return hover

# Selectors
def gen_pca_axis_selectors(pca_data, plot_data):
    select_x = Select(title="X",
                  value="PC1",
                  options=sorted(pca_data.column_names, key=lambda x: int(x[2:]))
                 )
    select_y = Select(title="Y",
                  value="PC2",
                  options=sorted(pca_data.column_names, key=lambda x: int(x[2:]))
                 )
    axis_callback = CustomJS(args=dict(plot_data=plot_data, pca_data=pca_data), code="""
        var data = plot_data.get("data");
        var axis = cb_obj["title"];
        var pcs = pca_data.get("data");
        var selection = cb_obj["value"];
        data[axis] = pcs[selection];
        plot_data.trigger("change");
    """)
    select_x.callback = axis_callback
    select_y.callback = axis_callback
    return select_x, select_y

def gen_color_selector(fields, plot_data, color_data, legend_data):
    select = Select(title="Color by:",
                    value=fields[0],
                    options=fields)
    select.callback = CustomJS(args=dict(plot_data=plot_data, color_data=color_data, legend_data=legend_data), code="""
        var selection = cb_obj["value"];
        var data = plot_data.get("data");
        var colors = color_data.get("data");
        var legend = legend_data.get("data");
        unique = function(a){
        	return a.reduce(function(res, current) {
        	if (!res.some(function(elem){ return elem == current})){
        		res.push(current)
        	}
        	return res
        	}, [])}
        data["color"] = colors[selection];
        data["coloredby"] = data[selection];
        legend["color"] = unique(data["color"])
        legend["coloredby"] = unique(data["coloredby"])
        legend["index"] = legend["coloredby"].map(function(_,i){ return legend["coloredby"].length - i; })
        legend_data.trigger("change")
        plot_data.trigger("change");
    """)
    return select

#####################################################
# Plots
#####################################################

def gen_pca_legend(legend_data):
    legend_labels = LabelSet(x=0, y="index", text="coloredby", source=legend_data, y_offset=-7.5, x_offset=12)
    legend = figure(title="Legend",
                    plot_width=300, plot_height=400,
                    x_range=Range1d(start=-0.5, end=10.),
                    tools=["ywheel_zoom", "tap", "reset"],
                    active_scroll="ywheel_zoom")
    legend.circle(0, "index", size=20, color="color", source=legend_data)
    legend.add_layout(legend_labels)
    legend.xaxis.visible = False
    legend.yaxis.visible = False
    legend.xgrid.visible = False
    legend.ygrid.visible = False
    return legend

def gen_pca_plots(targets, pca, plot_data):
    hover_pca = gen_hovertool(targets)
    scree = Bar(pd.Series(pca.explained_variance_, range(1, len(pca.explained_variance_)+1)),
            title="Scree Plot",
            legend=False,
            ylabel="Variance Explained",
            xlabel="Principal Component"
           )
    scree.height = 300
    pca_plot = figure(title="Interactive PCA",
                  plot_width=600,
                  plot_height=600,
                  tools=["box_select", "lasso_select","tap", "pan","box_zoom", hover_pca, "reset"],
                  active_drag="lasso_select",
                  active_scroll=None)
    pca_plot.circle("X", "Y", size=20, alpha=0.7, color="color", source=plot_data)
    return pca_plot, scree

def gen_box_plot(plot_data, targets):
    hover_box = gen_hovertool(targets)
    hover_box.tooltips.extend([(field, "@"+field) for field in ["Qmin", "Q1", "Q2", "Q3", "Qmax"]])
    box_plot = figure(height=400, width=800,
              x_range=list(plot_data.data["SampleID"]),
              y_range=Range1d(min(plot_data.data["Qmin"]), max(plot_data.data["Qmax"])),
              tools=["xwheel_zoom","reset","box_select","xpan","tap", hover_box],
              active_scroll=None)
    box_plot.circle("SampleID", "Q2", color="color", source=plot_data)
    box_plot.segment(x0="SampleID", x1="SampleID", y0="Q1", y1="Qmin", color="color", source=plot_data)
    box_plot.segment(x0="SampleID", x1="SampleID", y0="Q3", y1="Qmax", color="color", source=plot_data)
    box_plot.xaxis.major_label_orientation = pi/2
    return box_plot

#####################################################
# Main
#####################################################

def plot_dend(data, targets, fields=None):
    """
    Generates dendogram plot and connected color selector.

    usage:
        dend_plot, dend_selector = plot_dend(data, targets, fields)
    """
    if fields is None:
        fields = gen_fields(targets)
    #targets = targets[fields]
    targets = targets.reset_index(drop=True) # So I don't get indexing problems later (gen_dendcolor_ds)

    clusts = get_clusts(data)
    dend, groups = format_clusts(clusts)

    dend_color_dsource = gen_dendcolor_ds(groups, targets, fields)
    dend_plot_dsource = gen_dend_ds(groups, dend, targets, fields, dend_color_dsource)
    dend_hover = gen_dend_hover(targets, fields)
    color_selector = gen_dend_selector(fields, dend_plot_dsource, dend_color_dsource)
    axis_ds = ColumnDataSource(dict(
        position = list(range(5, len(dend["leaves"])*10+5, 10)),
        order = dend["leaves"],
        label = targets["SampleID"][dend["leaves"]])
    )

    dend_plot = figure(width=1000, height=600,
               tools = ["pan","box_zoom", "wheel_zoom", "reset", dend_hover]
              )
    dend_plot.multi_line("x", "y",color="color", source=dend_plot_dsource)
    dend_plot.xaxis.ticker = FixedTicker(ticks=list(range(5,len(dend["leaves"])*10+5, 10)))
    dend_plot.xaxis.formatter = FuncTickFormatter.from_coffeescript(
        args=dict(axis_dsource=axis_ds), # May want custom dsource
        code="""
        axis_data = axis_dsource.get("data")
        pos = axis_data["position"]
        idx = pos.indexOf(tick) if tick in pos
        axis_data["label"][idx]
    """)
    dend_plot.xaxis.major_label_orientation = pi/2

    return dend_plot, color_selector

def plot_qc(data, targets, fields=False):
    """
    Generates qc plots.

    usage:
        pca_plot, scree_plot, box_plot, color_select, select_x, select_y, legend = plot_qc(data, targets, fields)
    """
    expression = data[targets["SampleID"]]
    if not fields:
        fields = gen_fields(targets)
    #targets = targets[fields] # TODO A later call requires SampleID in targets, so either this should change or usage of fields should.

    if expression.isnull().any().any():
        # Probably give option to replace with zeros
        print("Removing null values.")
        expression = expression.dropna()

    pca = PCA(n_components=10)
    pca_fit = pca.fit_transform(expression.transpose())

    # Gen data sources
    color_data = gen_color_dsource(targets, fields)
    pca_data = gen_pca_dsource(pca_fit)
    plot_data = gen_plot_dsource(expression, targets, fields, pca_data, color_data)
    legend_data = gen_legend_dsource(plot_data)

    color_select = gen_color_selector(fields, plot_data, color_data, legend_data)
    select_x, select_y = gen_pca_axis_selectors(pca_data, plot_data)

    pca_plot, scree_plot = gen_pca_plots(targets, pca, plot_data)
    box_plot = gen_box_plot(plot_data, targets)
    legend = gen_pca_legend(legend_data)

    return pca_plot, scree_plot, box_plot, color_select, select_x, select_y, legend
    # return column(box_plot, row(color_select, select_x, select_y), pca_plot, scree_plot)

if __name__ == "__main__":
    import argparse
    from bokeh.plotting import output_file
    parser = argparse.ArgumentParser()
    parser.add_argument("expression", help="Expression file",
    type=Path)
    parser.add_argument("targets", help="Targets file.", type=Path)
    parser.add_argument("output", help="Output path", type=Path)
    args = parser.parse_args()

    output_file(str(args.output))

    expression = pd.read_table(args.expression, header=0, index_col=0)
    targets = pd.read_table(args.targets)

    # show(plot_qc(expression, targets))
    pca_plot, scree_plot, box_plot, color_select, select_x, select_y, legend = plot_qc(expression, targets)

    show(column(box_plot, row(pca_plot, column(color_select, select_x, select_y, legend)), scree_plot))
    # for i in (pca_plot, scree_plot, box_plot):
        # show(i)
