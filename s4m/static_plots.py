from math import pi
from functools import partial
from pathlib import Path
from sklearn.decomposition import PCA

import pandas as pd
import numpy as np
from bokeh.palettes import viridis

from .plotting import gen_color_map, gen_colors

import seaborn as sns
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches

def to_long(data, targets):
    """Converts count data from wide to long."""
    if data.columns.name == None:
        data = data.copy()
        sampleid = targets.columns[0]
        data.columns.name = sampleid
    else:
        sampleid = data.columns.name
    if data.index.name == None:
        data.index.name = "TranscriptID" # Maybe make this module level variable
    longdf = data.stack().reset_index(name="Counts")
    longdf = longdf.merge(targets, on=sampleid, how="left")
    return longdf

# TODO
# def to_wide(longdata, index_col="level_0"):
#     """Given expression data in long format, returns wide format data and targets"""
#     fields = list(longdata.columns)
#     fields.remove("Counts")
#     fields.remove(index_col)
#     data = longdata.pivot(index=index_col, columns="SampleID", values="Counts")
#     targets = longdata.groupby("SampleID")[fields].first()
#     # targets.reset_index(drop=True, inplace=True)
#     return data, targets

def plot_density(longdf, color_by, group="SampleID", counts="Counts", **kwargs):
    """Given long count data, plot density.

    Args:
        longdf: pd.DataFrame
        color_by: string
            Field to color by.
        group: string
            Field to group lines by. Should be subset of groupby.
        counts: string (opt)
            Field to use for counts.

    Returns:
        matplotlib.figure.Figure
    """
    colors = gen_color_map(longdf[color_by])
    legend_entries = []
    for name, subdf in longdf.groupby(group):
        assert len(subdf[color_by].unique()) == 1
        c = colors[subdf[color_by].iloc[0]]
        ax = sns.kdeplot(data=subdf[counts], color=c, gridsize=10000, legend=False) # Access this with ax.figure
    for n, c in colors.items():
        legend_entries.append(mpatches.Patch(color=c, label=n))
    ax.legend(handles=legend_entries)
    return ax#.get_figure() # Plots twice?

####################################################
# Static PCA plot
####################################################

def perform_pca(data):
    pca = PCA(n_components=10)
    fit = pca.fit_transform(data.transpose())
    fit = pd.DataFrame(fit,
                       columns=["PC{}".format(x) for x in range(1,11)],
                       index=data.columns)
    variance = pca.explained_variance_
    return fit, variance

def plot_pcs(ax, data, pc_x, pc_y, coloring):
    ax.scatter(data[pc_x], data[pc_y], c=coloring)
    ax.set_title("{} vs. {}".format(pc_x, pc_y))
    ax.set_xlabel(pc_x)
    ax.set_ylabel(pc_y)
    return ax

def plot_scree(ax, v):
    ax.plot(range(1, 11), v, marker='o', linestyle=":", fillstyle="none")
    ax.set_title("Scree plot")
    return ax

def gen_legend(fig, colorby, color_func=viridis, loc="center left"):
    cmap = gen_color_map(colorby, color_func)
    l = [mpatches.Patch(color=v, label=k) for k,v in cmap.items()]
    lgd = plt.legend(bbox_to_anchor=(1.05, 1.125), handles=l, loc=loc)
    fig.artists.append(lgd)
    return lgd # Seems a little heuristic

def plot_pca(data, t, title=None):
    if type(t) == pd.DataFrame:
        t = t["Contrasts"]
    fit, var = perform_pca(data)
    color_func = partial(sns.color_palette, "colorblind")
    colors = list(gen_colors(t, color_func))
    fig, ((scree, pc1v2), (pc2v3, pc1v3)) = plt.subplots(nrows=2, ncols=2, figsize=(10,10))
    lgd = gen_legend(fig, t, color_func) # TODO, return better results
    if not (title is None):
        ptitle = fig.suptitle(title)
        fig.artists.append(ptitle) # Makes this get considered during plotting
        fig.subplots_adjust(hspace=0.4)
    plot_scree(scree, var)
    plot_pcs(pc1v2, fit, "PC1", "PC2", colors)
    plot_pcs(pc2v3, fit, "PC2", "PC3", colors)
    plot_pcs(pc1v3, fit, "PC1", "PC3", colors)
#     fig.tight_layout() # Figures out nice spacing for plots, cuts off title
    fig.subplots_adjust(hspace=0.25)
    return fig
