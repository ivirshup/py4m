# py4m

This module contains most of the code I (Isaac) use to work with data from stemformatics in python. It has a few sections:

## `norm`

This module has utilities for normalizing RNA-seq data. This includes cpm and rpkm, and will call out to R for normalization factors from `edgeR`, `limma`, and `DESeq2`.

## `plotting`

Contains code for interactive pca, box, and dendogram plots. This has both convenience functions and lower level functions.

## `static_plots`

Currently contains:

* Density plots
* Static PCA
